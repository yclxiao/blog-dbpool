## 介绍
这个一个基于springboot的手脚架，搭建全过程参考

## 已集成或实现的功能

- 集成mysql数据库
- maven多环境配置
- maven多模块配置
- 集成mybatis
- 配置MyBatis Generator
- 集成pagehelper 分页插件
- 集成redis ,自定义 redis 操作类
- 集成spring cache
- 自定义全局状态码和业务结果类
- 统一异常处理
- 参数校验及异常处理
- 认证拦截器
- 统一映射自定义配置
- 配置日志
