package com.ycl.blog.exception;

import com.ycl.blog.enums.ApplicationEnum;


public class AppException extends RuntimeException {

    private static final long serialVersionUID = -1629701124990906510L;

    /**
     * 每一个抛出的异常都必须对应一个 ApplicationEnum
     */
    private ApplicationEnum applicationEnum;

    public ApplicationEnum getApplicationEnum() {
        return applicationEnum;
    }

    public AppException(ApplicationEnum applicationEnum) {
        super(applicationEnum.getCode() + " : " + applicationEnum.getMessage());
        this.applicationEnum = applicationEnum;
    }

    public AppException(String errorMsg) {
        super(errorMsg);
    }

}
