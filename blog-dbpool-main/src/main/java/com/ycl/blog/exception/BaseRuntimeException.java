package com.ycl.blog.exception;

import lombok.Getter;

/**
 * User: 杨成龙
 * Date: 2019/9/16
 * Time: 1:52 PM
 * Desc: 类描述
 */
public class BaseRuntimeException extends RuntimeException {
    private static final int DEFAULT_ERROR_CODE = 500;

    @Getter
    private int errCode;

    public BaseRuntimeException() {
        super();
    }

    public BaseRuntimeException(String message) {
        super(message);
        this.errCode = DEFAULT_ERROR_CODE;
    }

    public BaseRuntimeException(int errCode, String message) {
        super(message);
        this.errCode = errCode;
    }

    public BaseRuntimeException(String message, Throwable e) {
        super(message, e);
        this.errCode = DEFAULT_ERROR_CODE;
    }

    public BaseRuntimeException(int errCode, String message, Throwable cause) {
        super(message, cause);
        this.errCode = errCode;
    }

}
