package com.ycl.blog.config;

import lombok.Data;

/**
 * 阿里云OSS相关配置
 * 两种配置方式：直接在这里配置，或者统一在别的地方配置
 */
//@Configuration
//@ConfigurationProperties(prefix = "aliyun.oss")
@Data
public class AliOssConfigProperties {

    private String endpoint;

    private String accessKeyId;

    private String accessKeySecret;

    private String bucketName;
}