package com.ycl.blog.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * User: 杨成龙
 * Date: 2019/11/23
 * Time: 3:09 PM
 * Desc: 资源配置参数
 */
@Configuration("resourceConfigure")
public class ResourceConfigure {
    /**
     * ES集群配置参数
     *
     * @return 集群配置参数
     */
    @Bean
    @ConfigurationProperties(prefix = "aliyun.oss")
    public AliOssConfigProperties esConfigProperties() {
        return new AliOssConfigProperties();
    }

}
