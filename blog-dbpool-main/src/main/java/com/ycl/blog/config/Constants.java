package com.ycl.blog.config;

/**
 * User: 杨成龙
 * Date: 2020/3/7
 * Time: 3:25 下午
 * Desc: 常量类
 */
public interface Constants {
    /**
     * json格式登录身份
     */
    String JWT_JSON = "jwtJson";

    /**
     * token key
     */
    String JWT_SECRET = "jwtTestSecret";

    interface ExpTime {
        /**
         * User的超时时间，暂时设定两天
         */
        long UserExpTime = 30 * 24 * 60 * 60L;
    }

    /**
     * 用户角色
     */
    interface UserRole {
        /**
         * 管理员
         */
        Integer ADMIN = 1;
        /**
         * 普通用户
         */
        Integer NORMAL = 2;
    }
}
