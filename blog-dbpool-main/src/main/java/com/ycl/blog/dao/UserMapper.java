package com.ycl.blog.dao;

import com.ycl.blog.base.MyBaseMapper;
import com.ycl.blog.bean.condition.UserQueryCondition;
import com.ycl.blog.bean.po.UserPo;

public interface UserMapper extends MyBaseMapper<UserPo, UserQueryCondition> {

}