package com.ycl.blog.controller;

import com.ycl.blog.base.CentreCutPageResponse;
import com.ycl.blog.base.PageResponse;
import com.ycl.blog.base.result.Result;
import com.ycl.blog.base.result.SuccessResult;
import com.ycl.blog.bean.condition.UserQueryCondition;
import com.ycl.blog.bean.form.user.UserIdForm;
import com.ycl.blog.bean.form.user.UserQueryForm;
import com.ycl.blog.bean.po.UserPo;
import com.ycl.blog.bean.vo.UserVo;
import com.ycl.blog.service.UserService;
import com.ycl.blog.util.CopyUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@Api(tags = "用户相关接口")
@RequestMapping("/user")
@Slf4j
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * 获取用户信息
     * 参数通过url传递
     *
     * @param form
     * @return
     */
    @ApiOperation(value = "获取用户信息", notes = "获取用户信息", httpMethod = "POST")
    @PostMapping("/getUser")
    public Result<UserVo> getUser(@Valid @RequestBody UserIdForm form) {

        /*try {
            Thread.sleep(20000);
        } catch (InterruptedException e) {
            e.printStackTrace();
            throw new AppException("请求超时");
        }*/

        UserPo po = userService.getUser(form.getUserId());
        UserVo vo = new UserVo();
        if (po != null) {
            BeanUtils.copyProperties(po, vo);
            return new SuccessResult<>(vo);
        }
        return new SuccessResult<>(null);
    }

    @ApiOperation(value = "查询用户列表", notes = "查询用户列表", httpMethod = "POST")
    @PostMapping("/queryPageUser")
    public Result<CentreCutPageResponse<UserVo>> queryPageUser(@Valid @RequestBody UserQueryForm form) {
        UserQueryCondition condition = CopyUtil.transfer(form, UserQueryCondition.class);

        PageResponse<UserPo> pageResponse = userService.queryUserPage(condition);
        List<UserPo> userPoList = pageResponse.getDataList();
        List<UserVo> userVoList = CopyUtil.transfer(userPoList, UserVo.class);
        return new SuccessResult(new CentreCutPageResponse(form.getPageNum(), form.getPageSize(), pageResponse.getTotalCount(), userVoList));
    }

}