package com.ycl.blog.service;

import com.ycl.blog.base.BaseService;
import com.ycl.blog.base.PageResponse;
import com.ycl.blog.bean.condition.UserQueryCondition;
import com.ycl.blog.bean.po.UserPo;
import com.ycl.blog.config.ApplicationProperty;
import com.ycl.blog.dao.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

@Service
public class UserService extends BaseService<UserPo, UserQueryCondition, UserMapper> {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private ApplicationProperty applicationProperty;

    /**
     * 默认的加盐值
     * 默认密码：qwe123，md5：643839d202bf86ab383a650ec1825d44
     */
    private String salt = "testsalt";

    /**
     * 更加用户ID获取用户
     *
     * @param userId
     * @return
     */
    public UserPo getUser(String userId) {
        UserPo userPo = userMapper.selectByPrimaryKey(userId);
        return userPo;
    }

    /**
     * 分页查询用户
     *
     * @param condition
     * @return
     */
    public PageResponse<UserPo> queryUserPage(UserQueryCondition condition) {
        PageResponse<UserPo> pageResponse = this.queryPage(condition);
        return pageResponse;
    }

    /**
     * 根据用户名查询用户
     *
     * @param name
     * @return
     */
    private UserPo getUserByAccount(String account) {
        UserPo userPo = new UserPo();
        userPo.setUserAccount(account);
        return userMapper.selectOne(userPo);
    }

    /**
     * 加盐hash
     *
     * @param password
     * @param salt
     * @return
     */
    private static String addSaltHash(String password, String salt) {
        return DigestUtils.md5DigestAsHex((password + salt).getBytes());
    }

}
