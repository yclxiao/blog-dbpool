package com.ycl.blog.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
public @interface Identity {
    /**
     * 是否必须登录 默认是需要
     *
     * @return
     */
    boolean require() default true;
}
