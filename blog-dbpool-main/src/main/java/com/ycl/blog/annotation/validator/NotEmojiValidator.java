package com.ycl.blog.annotation.validator;

import com.ycl.blog.annotation.NotEmoji;
import com.ycl.blog.util.EmojiUtil;
import org.apache.commons.lang3.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;


/**
 * 表情符验证
 */
public class NotEmojiValidator implements ConstraintValidator<NotEmoji, String> {


    @Override
    public void initialize(NotEmoji emoji) {
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        if (StringUtils.isBlank(s)) {
            return true;
        }
        if (EmojiUtil.containsEmoji(s)) {
            return false;
        }
        return true;
    }

}
