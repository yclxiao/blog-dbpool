package com.ycl.blog.base.identity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

@Data
@NoArgsConstructor
public class IdentityUser {

    /**
     * 用户编号
     */
    private String userId;

    /**
     * 用户账号
     */
    private String userAccount;

    public IdentityUser(IdentityUser user) {
        BeanUtils.copyProperties(user, this);
    }
}
