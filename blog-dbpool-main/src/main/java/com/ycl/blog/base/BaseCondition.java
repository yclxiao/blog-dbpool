package com.ycl.blog.base;


import lombok.Data;
import lombok.ToString;

@Data
@ToString
public abstract class BaseCondition {

    private int pageSize = 0;
    private int pageNum = 0;
    private int skipResults = 0;
    private String orderBy;

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
        skipResults = pageSize * pageNum;
    }

    public void setPageNum(int pageNo) {
        this.pageNum = pageNo;
        skipResults = pageSize * pageNo;
    }


}
