package com.ycl.blog.base;

import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface MyBaseMapper<T, C extends BaseCondition> extends Mapper<T> {
    /**
     * 查询课程列表
     *
     * @return
     */
    List<T> selectList(C condition);

}