//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.ycl.blog.base;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CentreListResponse<T> implements Serializable {
    private static final long serialVersionUID = -7628952830016632166L;

    @ApiModelProperty("数据列表")
    private List<T> dataList;

}
