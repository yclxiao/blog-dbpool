package com.ycl.blog.base.result;


import com.ycl.blog.enums.ApplicationEnum;

public class FailResult<T> extends Result<T> {

    private static final long serialVersionUID = -6073157176763840816L;

    public FailResult() {
        setStatus(false);
        setCode(ApplicationEnum.FAIL.getCode());
        setMsg(ApplicationEnum.FAIL.getMessage());
    }

    public FailResult(ApplicationEnum applicationEnum) {
        setStatus(false);
        setCode(applicationEnum.getCode());
        setMsg(applicationEnum.getMessage());
    }


    public FailResult(ApplicationEnum applicationEnum, T data) {
        setStatus(false);
        setCode(applicationEnum.getCode());
        setMsg(applicationEnum.getMessage());
        setData(data);
    }

    public FailResult(String errorMsg) {
        setStatus(false);
        setCode(ApplicationEnum.UNKNOW_ERROR.getCode());
        setMsg(errorMsg);
        setData(null);
    }


}
