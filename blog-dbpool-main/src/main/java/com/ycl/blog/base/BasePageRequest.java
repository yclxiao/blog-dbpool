package com.ycl.blog.base;

import lombok.Data;


@Data
public abstract class BasePageRequest extends BaseRequest {
    /**
     * 页码
     */
    private int pageNum;
    /**
     * 条数 为0即查出全部
     */
    private int pageSize = 20;

    /**
     * 入参校验
     * - 校验失败时，统一返回 IllegalArgumentException 异常
     */
    public void checkParam() {
        super.checkParam();
    }

}
