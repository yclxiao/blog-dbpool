package com.ycl.blog.base;


import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class PageResponse<T> extends ListResponse<T> {
    private long totalCount;

    public PageResponse(long totalCount, List<T> dataList) {
        super(dataList);
        this.totalCount = totalCount;
    }
}
