package com.ycl.blog.base;

import lombok.Data;

import java.io.Serializable;


@Data
public abstract class BaseRequest implements Serializable {
    /**
     * 触发该请求的客户端IP地址
     */
    private String requestIp;

    /**
     * 触发该请求的系统：pc、ios、android、wechat、mini
     */
    private String systemSource;

    /**
     * 入参校验
     * - 校验失败时，统一返回 IllegalArgumentException 异常
     */
    public void checkParam() {

    }
}
