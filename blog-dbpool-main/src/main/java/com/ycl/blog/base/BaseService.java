package com.ycl.blog.base;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * User: 杨成龙
 * Date: 2020/3/7
 * Time: 2:37 下午
 * Desc: 类描述
 */
public class BaseService<T, C extends BaseCondition, M extends MyBaseMapper<T, C>> {

    @Autowired
    private M mapper;

    /**
     * 分页查询
     *
     * @param condition
     * @return
     */
    protected PageResponse<T> queryPage(C condition) {
        PageHelper.startPage(condition.getPageNum() + 1, condition.getPageSize());
        List<T> poList = mapper.selectList(condition);
        PageInfo<T> pageInfo = new PageInfo<>(poList);
        return new PageResponse<>(pageInfo.getTotal(), pageInfo.getList());
    }
}
