package com.ycl.blog.base;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 此处需要添加example，否则新版的swagger会有问题
 */
@Data
@ApiModel
public class BaseQueryForm {
    @ApiModelProperty(value = "分页每页条数", example = "10")
    public Integer pageSize = 10;

    @ApiModelProperty(value = "分页页码", example = "0")
    public Integer pageNum = 0;

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("BaseQueryForm{");
        sb.append("pageSize=").append(pageSize);
        sb.append(", pageNum=").append(pageNum);
        sb.append('}');
        return sb.toString();
    }
}
