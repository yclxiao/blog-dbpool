package com.ycl.blog.enums;

import lombok.Getter;

/**
 * 业务结果状态码
 */
@Getter
public enum ApplicationEnum {

    SUCCESS("200", "业务执行成功"),

    PARAMETER_BIND_FAIL("4000", "参数绑定失败"),
    PARAMETER_VERIFY_FAIL("4001", "参数校验失败"),

    NO_LOGIN("4100", "未登陆"),
    USER_NAME_REPETITION("4101", "用户名已存在"),
    USER_OR_PWD_ERR("4102", "用户名或密码错误"),
    USER_NO_EXIST("4103", "用户不存在"),
    YET_LOGIN("4104", "已登陆"),
    PASSWORD_ERR("4105", "密码错误"),
    TOKEN_INVALID("4106", "token失效"),

    REQUEST_FREQUENTLY("4201", "请求太过频繁"),

    FILE_OVER_SIZE("4301", "文件过大, 图片限制最大为20M"),

    VIDEO_OVER_SIZE("4302", "视频过大, 图片限制最大为50M"),

    FAIL("5000", "业务执行失败"),

    UNKNOW_ERROR("9999", "未知错误");

    /**
     * 设计原则
     * 200 业务执行成功
     * 4xxx 由于用户导致的错误，比如注册时，重复注册导致的注册失败
     * 5xxx 由于系统原因导致的错误
     */
    private String code;

    private String message;

    ApplicationEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

}
