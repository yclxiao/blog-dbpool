package com.ycl.blog.bean.form.user;

import com.ycl.blog.base.BaseQueryForm;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * User: 杨成龙
 * Date: 2020/3/7
 * Time: 2:00 下午
 * Desc: 用户列表查询
 */
@ApiModel(value = "列表表单", description = "列表表单")
@Data
public class UserQueryForm extends BaseQueryForm {

}
