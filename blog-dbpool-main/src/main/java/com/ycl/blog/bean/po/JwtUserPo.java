package com.ycl.blog.bean.po;

import lombok.Data;

/**
 * Created by yangchenglong on 2018/1/10.
 */
@Data
public class JwtUserPo {
    /**
     * 用户账号
     */
    private String userAccount;

    /**
     * 用户id
     */
    private String userId;
}
