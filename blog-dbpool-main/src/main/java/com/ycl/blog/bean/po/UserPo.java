package com.ycl.blog.bean.po;

import com.ycl.blog.config.Constants;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Table(name = "tb_user")
public class UserPo {
    /**
     * 用户主键
     */
    @Id
    @Column(name = "user_id")
    private String userId;

    /**
     * 用户注册账户
     */
    @Column(name = "user_account")
    private String userAccount;

    /**
     * 用户名称
     */
    @Column(name = "user_name")
    private String userName;

    /**
     * 用户手机号
     */
    @Column(name = "user_phone")
    private String userPhone;

    /**
     * 用户密码
     */
    @Column(name = "user_pwd")
    private String userPwd;

    /**
     * 添加时间
     */
    @Column(name = "add_time")
    private Date addTime;

    /**
     * 添加人编号
     */
    @Column(name = "add_user")
    private String addUser;

    /**
     * 用户角色 1管理员  2普通人员
     */
    @Column(name = "user_role")
    private Integer userRole;

    /**
     * 构建普通用户
     *
     * @param account
     * @param pwd
     * @param userName
     * @param addUser
     */
    public void buildNormalUser(String userId, String account, String pwd, String userName, String addUser) {
        this.setUserId(userId);
        this.setUserAccount(account);
        this.setUserPwd(pwd);
        this.setUserName(userName);
        this.setUserPhone(account);
        this.setAddUser(addUser);
        this.setAddTime(new Date());
        this.setUserRole(Constants.UserRole.NORMAL);
    }
}