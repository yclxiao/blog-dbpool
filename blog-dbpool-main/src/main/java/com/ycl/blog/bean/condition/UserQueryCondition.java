package com.ycl.blog.bean.condition;

import com.ycl.blog.base.BaseCondition;

/**
 * User: 杨成龙
 * Date: 2020/3/7
 * Time: 2:04 下午
 * Desc: 用户相关查询
 */
public class UserQueryCondition extends BaseCondition {
    /**
     * 用户账户
     */
    private String userAccount;
}
