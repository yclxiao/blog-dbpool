package com.ycl.blog.bean.form.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@ApiModel(value = "用户ID", description = "用户ID")
@Data
public class UserIdForm implements Serializable {

    @ApiModelProperty(value = "用户ID", required = true)
    @NotBlank(message = "用户ID")
    private String userId;

}
