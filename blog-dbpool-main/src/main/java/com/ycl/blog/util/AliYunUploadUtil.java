package com.ycl.blog.util;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.ycl.blog.config.AliOssConfigProperties;
import lombok.extern.slf4j.Slf4j;

import java.io.InputStream;

/**
 * User: 杨成龙
 * Date: 2020/3/8
 * Time: 3:49 下午
 * Desc: 上传文件到阿里云
 */
@Slf4j
public class AliYunUploadUtil {

    /**
     * 上传文件到阿里云
     *
     * @param fileDir
     * @param fileName
     * @param inputStream
     * @return
     */
    public static String uploadToServer(AliOssConfigProperties properties, String fileDir, String fileName, InputStream inputStream) {
        String endpoint = properties.getEndpoint();
        String accessKeyId = properties.getAccessKeyId();
        String accessKeySecret = properties.getAccessKeySecret();
        String bucketName = properties.getBucketName();
        // <yourObjectName>上传文件到OSS时需要指定包含文件后缀在内的完整路径，例如abc/efg/123.jpg。
        String objectName = fileDir + "/" + fileName;

        // 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);

        // 上传内容到指定的存储空间（bucketName）并保存为指定的文件名称（objectName）。
        ossClient.putObject(bucketName, objectName, inputStream);

        // 关闭OSSClient。
        ossClient.shutdown();

        return "http://" + bucketName + "." + endpoint + "/" + objectName;
    }
}
