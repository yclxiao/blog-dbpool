package com.ycl.blog.util;

import com.ycl.blog.base.PageResponse;
import com.ycl.blog.exception.BaseRuntimeException;
import org.springframework.beans.BeanUtils;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class CopyUtil {
    /**
     * bean转为另一个bean
     *
     * @param source
     * @param targetClass
     * @param <T>
     * @return
     */
    public static <T> T transfer(Object source, Class<T> targetClass) {
        if (source == null) {
            return null;
        }
        try {
            T t = targetClass.newInstance();
            BeanUtils.copyProperties(source, t);
            return t;
        } catch (InstantiationException e) {
            throw new BaseRuntimeException("拷贝bean有误：" + e.getMessage(), e);
        } catch (IllegalAccessException e) {
            throw new BaseRuntimeException("拷贝bean有误：" + e.getMessage(), e);
        }
    }


    /**
     * 泛型为一种bean的list转为另一种泛型bean的list
     *
     * @param sourceList
     * @param targetClass
     * @param <T>
     * @return
     */
    public static <T> List<T> transfer(List<?> sourceList, Class<T> targetClass) {
        if (sourceList == null) {
            return null;
        }
        return sourceList.stream().map((source) -> transfer(source, targetClass)).collect(Collectors.toList());
    }

    /**
     * 泛型为一种bean的PageResponse转为另一种泛型bean的PageResponse
     *
     * @param sourcePageResponse
     * @param targetClass
     * @param <T>
     * @return
     */
    public static <T> PageResponse<T> transfer(PageResponse<?> sourcePageResponse, Class<T> targetClass) {
        if (sourcePageResponse == null) {
            return null;
        }
        List<T> targetList = transfer(sourcePageResponse.getDataList(), targetClass);
        return new PageResponse<>(sourcePageResponse.getTotalCount(), targetList);
    }

    /**
     * map转换为bean
     *
     * @param map
     * @param targetClass
     * @param <T>
     * @return
     */
    public static <T> T mapTransferBean(Map map, Class<T> targetClass) {
        if (map == null) {
            return null;
        }
        try {
            T t = targetClass.newInstance();
            org.apache.commons.beanutils.BeanUtils.populate(t, map);
            return t;
        } catch (InstantiationException e) {
            throw new BaseRuntimeException("map转换为bean：" + e.getMessage(), e);
        } catch (IllegalAccessException e) {
            throw new BaseRuntimeException("map转换为bean：" + e.getMessage(), e);
        } catch (InvocationTargetException e) {
            throw new BaseRuntimeException("map转换为bean：" + e.getMessage(), e);
        }
    }

    /**
     * bean转换为map
     *
     * @param bean
     * @return
     */
    public static Map<String, String> beanTransferMap(Object bean) {
        try {
            Map<String, String> map = org.apache.commons.beanutils.BeanUtils.describe(bean);
            return map;
        } catch (IllegalAccessException e) {
            throw new BaseRuntimeException("bean转换为map：" + e.getMessage(), e);
        } catch (InvocationTargetException e) {
            throw new BaseRuntimeException("bean转换为map：" + e.getMessage(), e);
        } catch (Exception e) {
            throw new BaseRuntimeException("bean转换为map：" + e.getMessage(), e);
        }
    }

}
