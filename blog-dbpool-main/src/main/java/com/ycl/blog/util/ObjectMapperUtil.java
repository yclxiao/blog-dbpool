package com.ycl.blog.util;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ycl.blog.exception.AppException;

import java.util.List;


public class ObjectMapperUtil {
    protected static ObjectMapper mapper = new ObjectMapper();

    static {

        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, false);

    }

    /**
     * 把json字符串反序列化为指定T类型的java类
     *
     * @param content
     * @param valueType
     * @param <T>
     * @return
     */
    public static <T> T readValue(String content, Class<T> valueType) {
        try {
            return mapper.readValue(content, valueType);
        } catch (Exception e) {
            throw new AppException("转换为json出错,内容如下：" + content);
        }
    }

    /**
     * 把json字符串反序列化为指定TypeReference类型，支持List、嵌套javabean等复杂类型
     *
     * @param content
     * @param typeReference
     * @param <T>
     * @return
     */
    public static <T> T readValue(String content, TypeReference<T> typeReference) {
        try {
            return mapper.readValue(content, typeReference);
        } catch (Exception e) {
            throw new AppException("转换为json出错,内容如下：" + content);
        }
    }

    /**
     * 把json字符串反序列化为指定T类型的list类
     *
     * @param content
     * @param valueType
     * @param <T>
     * @return
     */
    public static <T> List<T> readValueList(String content, Class<T> valueType) {
        JavaType javaType = mapper.getTypeFactory().constructCollectionType(List.class, valueType);
        try {
            return mapper.readValue(content, javaType);
        } catch (Exception e) {
            throw new AppException("转换为json出错,内容如下：" + content);
        }
    }

    /**
     * 把java对象序列化成json字符串
     *
     * @param value
     * @return
     */
    public static String writeValueAsString(Object value) {
        try {
            return mapper.writeValueAsString(value);
        } catch (Exception e) {
            throw new AppException("转换为字符串出错");
        }
    }
}
