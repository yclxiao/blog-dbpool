package com.ycl.blog.util;

import java.util.List;
import java.util.stream.Collectors;

public class DepthCopyUtil {

    /**
     * 深度拷贝bean转为另一个bean
     *
     * @param source
     * @param targetClass
     * @param <T>
     * @return
     */
    public static <T> T transfer(Object source, Class<T> targetClass) {
        if (source == null) {
            return null;
        }
        String json = ObjectMapperUtil.writeValueAsString(source);
        return ObjectMapperUtil.readValue(json, targetClass);
    }


    /**
     * 泛型为一种bean的list转为另一种泛型bean的list
     *
     * @param source
     * @param targetClass
     * @param <T>
     * @return
     */
    public static <T> List<T> transfer(List<?> source, Class<T> targetClass) {
        if (source == null) {
            return null;
        }
        return source.stream().map((s) -> transfer(s, targetClass)).collect(Collectors.toList());
    }

}
