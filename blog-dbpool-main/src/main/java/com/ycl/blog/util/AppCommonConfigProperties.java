package com.ycl.blog.util;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * User: 杨成龙
 * Date: 2019/11/23
 * Time: 2:12 PM
 * Desc: app通用配置
 */
@Configuration
@ConfigurationProperties(prefix = "app.common")
@Data
public class AppCommonConfigProperties {
    /**
     * 机器ID，分配ID时使用
     */
    private long machineId = 1L;
}
