-- 创建 boot 数据库
CREATE DATABASE
IF
	NOT EXISTS testdb DEFAULT CHARSET utf8 COLLATE utf8_bin;

-- 选择 boot 数据库
USE testdb;

-- 创建 user 表
SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;
DROP TABLE
IF
	EXISTS `tb_user`;

-- 用户表
CREATE TABLE `testdb`.`tb_user`  (
  `user_id` varchar(64) NOT NULL COMMENT '用户主键',
  `user_account` varchar(64) NOT NULL COMMENT '用户注册账户',
  `user_name` varchar(64) NULL COMMENT '用户名称',
  `user_phone` varchar(11) NOT NULL COMMENT '用户手机号',
  `user_pwd` varchar(64) NOT NULL COMMENT '用户密码',
  `add_time` datetime(0) NOT NULL COMMENT '添加时间',
  `add_user` varchar(64) NOT NULL COMMENT '添加人编号',
  `user_role` int(2) NOT NULL COMMENT '用户角色 1管理员  2普通人员',
  PRIMARY KEY (`user_id`)
);

-- 增加索引
ALTER TABLE `testdb`.`tb_user`
ADD UNIQUE INDEX `idx_union_account`(`user_account`) USING BTREE,
ADD UNIQUE INDEX `idx_union_phone`(`user_phone`) USING BTREE;


-- 新增语句
INSERT INTO tb_user(user_id,user_account,user_name,user_phone,user_pwd,add_time,add_user,user_role)
VALUES
('U553844920155115520','15850501595','管理员','15850501595','643839d202bf86ab383a650ec1825d44',NOW(),'0',1);